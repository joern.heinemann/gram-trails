<?php
namespace Gram\Trails\Strategy;

use Gram\Resolver\ResolverInterface;
use Gram\Strategy\StrategyInterface;
use Gram\Strategy\StrategyTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class TrailsStrategy
 * @package Gram\Trails\Strategy
 *
 * Wandelt den Trails_Response zu Psr 7 Response um
 */
class TrailsStrategy implements StrategyInterface
{
	use StrategyTrait;

	/**
	 * @inheritdoc
	 */
	public function invoke(
		ResolverInterface $resolver,
		array $param,
		ServerRequestInterface $request,
		ResponseInterface $response
	): ResponseInterface
	{
		$this->prepareResolver($request,$response,$resolver);

		\ob_start();
		$level = \ob_get_level();

		$content = $resolver->resolve($param);

		while (\ob_get_level() >= $level) {
			\ob_end_flush();
		}

		if($content instanceof \Trails_Response) {
			return $this->createResponse($content,$response);
		}

		return $this->createBody($resolver,$content);
	}

	/**
	 * @inheritdoc
	 */
	protected function getContentTypeHeader(): array
	{
		return ['Content-Type','text/html'];
	}

	/**
	 * Wandelt den Trails_Response in psr7 Response um
	 *
	 * @param \Trails_Response $trailsResponse
	 * @param ResponseInterface $psr7
	 * @return ResponseInterface
	 */
	private function createResponse(\Trails_Response $trailsResponse, ResponseInterface $psr7)
	{
		$status = $trailsResponse->status;

		if($status !== null){
			$psr7 = $psr7->withStatus($status);
		}

		foreach ((array) $trailsResponse->headers as $k => $v) {
			$psr7 = $psr7->withHeader($k,$v);
		}

		$psr7->getBody()->write($trailsResponse->body);

		return $psr7;
	}
}