<?php
namespace Gram\Trails\Resolver;

use Gram\ResolverCreator\ResolverCreator;

class TrailsResolverCreator extends ResolverCreator
{
	/** @var \Trails_Dispatcher */
	private $trails_Dispatcher;

	/**
	 * @var string
	 *
	 * der class name für den Resolver
	 */
	private $resolverClassName;

	/**
	 * TrailsResolverCreator constructor.
	 * @param \Trails_Dispatcher $trails_Dispatcher
	 * @param string $resolverClassName
	 */
	public function __construct(\Trails_Dispatcher $trails_Dispatcher,string $resolverClassName)
	{
		$this->trails_Dispatcher = $trails_Dispatcher;
		$this->resolverClassName = $resolverClassName;
	}

	/**
	 * @inheritdoc
	 */
	public function createResolver($possibleCallable)
	{
		//erstelle den Trails Resolver
		if(\is_array($possibleCallable)) {
			/** @var \Gram\Resolver\ResolverInterface|TrailsControllerResolver $resolver */
			$resolver = new $this->resolverClassName($this->trails_Dispatcher);

			$resolver->set($possibleCallable);

			return $resolver;
		}

		//sonst erstelle die Std Resolver
		return parent::createResolver($possibleCallable);
	}
}