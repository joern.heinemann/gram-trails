<?php
namespace Gram\Trails\Resolver;

use Gram\Exceptions\CallableNotAllowedException;
use Gram\Resolver\ResolverInterface;
use Gram\Resolver\ResolverTrait;

/**
 * Class TrailsControllerResolver
 * @package Gram\Trails\Resolver
 *
 * Einfacher Resolver für Trails
 *
 * Führt perform im Trails Controller aus
 *
 * Ersetzt den Trails_Dispatcher
 *
 * Wird bereits mit controller, file und function aufgerufen
 */
class TrailsControllerResolver implements ResolverInterface
{
	use ResolverTrait;

	private $controller;

	private $file;

	private $function;

	/** @var \Trails_Dispatcher */
	private $trails_Dispatcher;

	public function __construct(\Trails_Dispatcher $trails_Dispatcher)
	{
		$this->trails_Dispatcher = $trails_Dispatcher;
	}

	/**
	 * @inheritdoc
	 * @throws \Trails_UnknownController
	 *
	 * ersetzt im Trails_Dispatcher die map Method
	 */
	public function resolve(array $param)
	{
		require_once "{$this->trails_Dispatcher->trails_root}/controllers/{$this->file}.php";

		if (!class_exists($this->controller)) {
			throw new \Trails_UnknownController("Controller missing: '$this->controller'");
		}

		\array_unshift($param,$this->function);	//füge die Function als Parameter hinzu fügr perform

		/** @var \Trails_Controller $class */
		$class = new $this->controller($this->trails_Dispatcher);

		$response = $class->perform(\implode("/",$param));

		return $response;
	}

	/**
	 * @inheritdoc
	 *
	 * @throws CallableNotAllowedException
	 */
	public function set(array $controller = []): void
	{
		if(empty($controller)) {
			throw new CallableNotAllowedException("TrailsController must be an array");
		}

		[$this->controller,$this->file,$this->function] = $controller;
	}
}