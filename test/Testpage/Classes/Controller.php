<?php
namespace Gram\Trails\Test\Testpage\Classes;

use Gram\Middleware\Classes\ClassInterface;
use Gram\Middleware\Classes\ClassTrait;

class Controller implements ClassInterface
{
	use ClassTrait;

	public function index()
	{
		return "Hello World";
	}

	public function indexWithParam($id)
	{
		return $id;
	}
}