<?php

include_once "../../vendor/autoload.php";

include_once "env.php";

include_once "../../vendor/luniki/flexi-templates/lib/flexi.php";

use Gram\Project\App\AppFactory as App;
use Gram\Trails\Resolver\TrailsResolverCreator;
use Gram\Trails\Strategy\TrailsStrategy;

$trails_dispatcher = new Trails_Dispatcher(__DIR__,"","");

$resolver = 'Gram\\Trails\\Resolver\\TrailsControllerResolver';

App::setBase(BASE_PATH);	//BasePath, anpassen in der env.php

App::setResolverCreator(new TrailsResolverCreator($trails_dispatcher,$resolver));

//Routes definieren
App::get("/",function (){
	return "Test Trails: <a href='".BASE_PATH."/foo/index'>starten</a> <br> 
			Test Trails with Param: <a href='".BASE_PATH."/foo/index/123'>starten</a> <br><br>
			Test Gram: <a href='".BASE_PATH."/controller'>starten</a> <br>
			Test Gram with Param: <a href='".BASE_PATH."/controller/123'>starten</a>";
});

use Gram\Trails\Test\Testpage\Classes\Controller;

App::get("/controller",Controller::class."@index");
App::get("/controller/{id}",Controller::class."@indexWithParam");

//Trails Routes
App::group("",function (){
	App::get("/foo/index",[FooController::class,"foo","index"]);
	App::get("/foo/index/{id}",[FooController::class,"foo","indexWithParam"]);
	App::get("/foo/tpl",[FooController::class,"foo","indexTpl"]);
})->addStrategy(new TrailsStrategy());


$factory = new \Nyholm\Psr7\Factory\Psr17Factory();	//Psr 17 Factory

App::setResponseFactory($factory);

$requestCreator = new \Nyholm\Psr7Server\ServerRequestCreator(
	$factory,
	$factory,
	$factory,
	$factory
);

$request = $requestCreator->fromGlobals();

App::start($request);