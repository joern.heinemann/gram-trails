<?php

class FooController extends \Trails_Controller
{
	/**
	 * Gibt einen Text aus
	 *
	 * @throws Trails_DoubleRenderError
	 */
	public function index_action()
	{
		$this->render_text('Successfully');
	}

	/**
	 * Rendert ein Tpl
	 */
	public function indexTpl_action()
	{

	}

	/**
	 * Gibt ein Parameter aus
	 *
	 * @param $id
	 * @throws Trails_DoubleRenderError
	 */
	public function indexWithParam_action($id)
	{
		$this->render_text($id);
	}
}