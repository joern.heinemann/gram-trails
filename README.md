# phpgram trails

````php
<?php

include_once "vendor/autoload.php";

include_once "vendor/luniki/flexi-templates/lib/flexi.php";

use Gram\Project\App\AppFactory as App;
use Gram\Trails\Resolver\TrailsResolverCreator;
use Gram\Trails\Strategy\TrailsStrategy;

//Trails_Dispatcher for controller
$trails_dispatcher = new Trails_Dispatcher(__DIR__,"","");

//This Resolver will be created
$resolver = 'Gram\\Trails\\Resolver\\TrailsControllerResolver';

App::setResolverCreator(new TrailsResolverCreator($trails_dispatcher,$resolver));

//normal Routes

App::get("/",function (){
	return "Landing Page";
});

App::get("/index/{id}",IndexController::class."@index");

//Trails Routes
App::group("",function (){
	//Array as handler: class name, file name, method name
	App::get("/foo/index",["FooController","foo","index"]);
	App::get("/foo/index/{id}",["FooController","foo","indexWithParam"]);
})->addStrategy(new TrailsStrategy());	//new Strategy for Trails_Response

/*
 * Page start
 */

$factory = new \Nyholm\Psr7\Factory\Psr17Factory();	//Psr 17 Factory

App::setResponseFactory($factory);

$requestCreator = new \Nyholm\Psr7Server\ServerRequestCreator(
	$factory,
	$factory,
	$factory,
	$factory
);

$request = $requestCreator->fromGlobals();

App::start($request);
````